# Demo package for shiny apps

This package contains a shiny app in the inst/apps folder and
a function to launch that app in the R/ folder.

Also there's a script in the inst/scripts folder which should run
the shiny app when run from the command line or launched from a
file explorer.

